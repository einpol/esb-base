ESB Microframework Distribution
===============================
Welcome to the ESB Framework Distribution - a fully-functional **Symfony
Microframework** application that you can use as a skeleton for new ESB implementations.

For more information about the **Symfony Microframework** and the MicroKernel trait visit the
[Symfony as a Microframework][1] chapter of the Symfony Documentation.

What's inside?
--------------
The ESB Framework Foundation is configured with the following defaults:
  * A **Symfony Microframework** with minimal configuration
  * The [EsbCoreBundle][2]providing esb´s core functionality
  * An App Bundle for starting your implementation.

Installation
------------
To install the ESB Framework Distribution please use composer´s `create-project` command 
in combination with our internal [satis][3] repository.

```composer create-project reply/esb --repository-url=http://composer.pttde.de```

Alternatively you can always clone the repo and use `composer install` to install 
all necessary dependencies.

Todo´s
------
  * Semantic Versioning
  * Changelog
  * testing, testing... and more testing

Enjoy!

[1]:  https://symfony.com/blog/new-in-symfony-2-8-symfony-as-a-microframework
[2]:  https://gitlab.pttde.de/esb/core
[3]:  https://composer.pttde.de